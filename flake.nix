{
  description = "alternative-backend";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, haskellNix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" "x86_64-darwin" ] (system:
      let
        overlays = [ haskellNix.overlay
        (final: prev: {
          # This overlay adds our project to pkgs
          alternative-backend =
            final.haskell-nix.project' {
              src = ./.;
              compiler-nix-name = "ghc8107";
              shell = {
                  buildInputs = with pkgs; [
                    nixpkgs-fmt
                    heroku
                  ];
                tools = {
                  cabal = {};
                  hlint = {};
                  haskell-language-server = {};
                };
                crossPlatform = [];
              };
            };
          }) 
        ];
      pkgs = import nixpkgs { inherit system overlays; inherit (haskellNix) config; };
      flake = pkgs.alternative-backend.flake { crossPlatforms = p: []; };
      package = flake.packages."alternative-backend:exe:alternative-backend";

      in flake // {
        defaultPackage = package;
        packages.docker-image = pkgs.dockerTools.buildImage{

          name = "alternative-backend";
          tag = "latest";
          contents = with pkgs; [
            package
            busybox
          ];

          config = {
            Cmd = [ "/bin/alternative-backend" ];
          };
        };
      });
}
