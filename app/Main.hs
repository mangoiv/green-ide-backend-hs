module Main where

import           Data.Maybe
import           Network.Wai.Handler.Warp
import           Server
import           System.Environment.Blank

main :: IO ()
main = do
  putStrLn "Server starting up"
  port <- read . fromMaybe "8080" <$> getEnv "PORT"
  putStrLn $ "Listening on port " ++ show port
  run port app
