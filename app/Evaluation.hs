{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Evaluation
  ( Evaluation (..)
  , EvaluationEntry (..)
  , Severity (..)
  , sampleEvaluation
  , Evaluatable (..)
  ) where

import           Data.Aeson
import           Data.Text
import           Data.UUID
import           GHC.Generics

data Severity = OKAY
              | WARN
              | CRITICAL
              | BAD
              deriving (Eq, Show, Generic)

data EvaluationEntry = EvaluationEntry
  { description :: Text
  , severity    :: Severity
  , start       :: (Int, Int)
  , end         :: (Int, Int)
  }
  deriving (Eq, Show, Generic)

data Evaluation = Evaluation
  { evaluation :: [EvaluationEntry]
  , requestee  :: UUID}
  deriving (Eq, Show, Generic)

class Evaluatable a where
  evaluate :: a -> Evaluation

instance ToJSON Severity
instance ToJSON EvaluationEntry
instance ToJSON Evaluation

sampleEvaluation :: UUID -> Evaluation
sampleEvaluation uuid = Evaluation { requestee = uuid
                                   , evaluation =
                                     [ EvaluationEntry
                                       { description = "This is a short description"
                                       , severity = OKAY
                                       , start = (1,1)
                                       , end = (1,20)
                                       }
                                     , EvaluationEntry
                                       { description = "Another short description"
                                       , severity = BAD
                                       , start = (10,20)
                                       , end = (20,30)
                                       }
                                     ]}
