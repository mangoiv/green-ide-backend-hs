{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}
module Server where

import           Control.Monad.IO.Class
import           CsvParsing
import           Data.Aeson
import qualified Data.ByteString.Lazy   as Bs
import           Data.Text
import           Data.UUID
import qualified Evaluation             as Ev
import qualified EvaluationRequest      as Er
import           GHC.Generics
import           Servant
import qualified Token                  as Tk
import           Types


type EvaluationAPI = "token" :> Get '[JSON] Tk.Token
                :<|> "evaluation" :> ReqBody '[JSON] Er.EvaluationRequest :> Post '[JSON] Ev.Evaluation

evaluationAPI :: Proxy EvaluationAPI
evaluationAPI = Proxy

server :: Server EvaluationAPI
server = serveToken :<|> serveEvaluation
  where
    serveToken :: Handler Tk.Token
    serveToken = do
      uuid <- liftIO Tk.getRandomUUID
      return Tk.Token { Tk.token = uuid
                      }
    serveEvaluation :: Er.EvaluationRequest -> Handler Ev.Evaluation
    serveEvaluation Er.MkEvaluationRequest {..} = do
      csvData <- liftIO $ Bs.readFile "assets/model_kanzi_method_level.csv"
      let m = buildMapForMethodName <$> buildModel csvData
      liftIO $ print m
      return (Ev.sampleEvaluation requestToken)

app :: Application
app = serve evaluationAPI server
