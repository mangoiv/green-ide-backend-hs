{-# LANGUAGE DeriveGeneric #-}
module Types ( KanziConfigAndData (..)
             , KanziConfiguration (..)
             , KanziData (..)
             , KanziMethodName (..)
             , KanziModel (..)
             , KanziModelEntry (..)
             , EnergyInMs (..)
             , RunTimeInMs (..)
             ) where

import           Data.MultiMap (MultiMap)
import           Data.Text     (Text)
import           GHC.Generics

newtype KanziMethodName = MkKanziMethodName Text
  deriving (Ord, Show, Eq, Generic)

newtype EnergyInMs = MkEnergyInMs Double
  deriving (Ord, Show, Eq, Generic)

newtype RunTimeInMs = MkRunTimeInMs Double
  deriving (Ord, Show, Eq, Generic)

data KanziModelEntry = MkKanziModelEntry
  { methodName    :: !KanziMethodName
  , configuration :: !KanziConfiguration
  , evaluation    :: !KanziData
  }
  deriving (Show, Generic, Eq)

data KanziData = MkKanziData
  { runTimeInMs :: !RunTimeInMs
  , energyInmWs :: !EnergyInMs
  }
  deriving (Show, Generic, Eq)

data KanziConfiguration = MkKanziConfiguration
  { root        :: !Bool
  , blocksize   :: !Bool
  , jobs        :: !Bool
  , level       :: !Bool
  , checksum    :: !Bool
  , skip        :: !Bool
  , noTransform :: !Bool
  , huffman     :: !Bool
  , ans0        :: !Bool
  , ans1        :: !Bool
  , range       :: !Bool
  , fpaq        :: !Bool
  , tpaq        :: !Bool
  , cm          :: !Bool
  , noEntropy   :: !Bool
  , bwts        :: !Bool
  , rolz        :: !Bool
  , rlt         :: !Bool
  , zrlt        :: !Bool
  , mtft        :: !Bool
  , rank        :: !Bool
  , text        :: !Bool
  , x86         :: !Bool
  }
  deriving (Show, Generic, Eq)

type KanziConfigAndData = (KanziConfiguration, KanziData)

data KanziModel = MultiMap KanziMethodName KanziModelEntry

