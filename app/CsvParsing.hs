{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE StandaloneDeriving         #-}
module CsvParsing ( buildModel
                  , buildMapForConfigField
                  , buildMapForMethodName
                  ) where

import           Control.Monad
import           Data.ByteString.Lazy (ByteString)
import           Data.Csv
import           Data.MultiMap        (MultiMap, empty, fromList, insert, toMap)
import           Data.Vector          (Vector, toList)
import qualified Data.Vector          as V
import           Types

deriving instance FromField KanziMethodName
deriving instance FromField EnergyInMs
deriving instance FromField RunTimeInMs

instance FromField Bool where
  parseField s
    | s == "0" = pure False
    | s == "1" = pure True
    | otherwise = mzero

instance FromNamedRecord KanziModelEntry where
  parseNamedRecord r = MkKanziModelEntry <$>
      r .: "method_name" <*>
      ( MkKanziConfiguration <$>
        r .: "root" <*>
        r .: "BLOCKSIZE" <*>
        r .: "JOBS" <*>
        r .: "LEVEL" <*>
        r .: "CHECKSUM" <*>
        r .: "SKIP" <*>
        r .: "NoTransform" <*>
        r .: "Huffman" <*>
        r .: "ANS0" <*>
        r .: "ANS1" <*>
        r .: "Range" <*>
        r .: "FPAQ" <*>
        r .: "TPAQ" <*>
        r .: "CM" <*>
        r .: "NoEntropy" <*>
        r .: "BWTS" <*>
        r .: "ROLZ" <*>
        r .: "RLT" <*>
        r .: "ZRLT" <*>
        r .: "MTFT" <*>
        r .: "RANK" <*>
        r .: "TEXT"<*>
        r .: "X86"
      ) <*>
      ( MkKanziData <$>
        r .: "run_time(ms;<)" <*>
        r .: "energy(mWs;<)"
      )

instance DefaultOrdered KanziModelEntry where
  headerOrder _ = header [ "method_name","root","BLOCKSIZE","JOBS","LEVEL","CHECKSUM","SKIP","NoTransform"
                         , "Huffman","ANS0","ANS1","Range","FPAQ","TPAQ","CM","NoEntropy","BWTS","ROLZ"
                         , "RLT","ZRLT","MTFT","RANK","TEXT","X86","run_time(ms;<)","energy(mWs;<)"
                         ]

instance (Show k, Show v) => Show (MultiMap k v) where
  show = show . toMap

buildModel :: ByteString -> Either String (Vector KanziModelEntry)
buildModel = fmap snd . decodeByName

buildMapForMethodName :: Vector KanziModelEntry -> MultiMap KanziMethodName KanziConfigAndData
buildMapForMethodName = fromList . toList . fmap toTup
  where
    toTup :: KanziModelEntry -> (KanziMethodName, KanziConfigAndData)
    toTup MkKanziModelEntry {..} = (methodName, (configuration,evaluation))

buildMapForConfigField :: Ord a => (KanziConfiguration -> a) -> Vector KanziModelEntry -> MultiMap a KanziConfiguration
buildMapForConfigField f v = foldl extractField empty (configuration <$> v)
  where
    extractField m e = insert (f e) e m
