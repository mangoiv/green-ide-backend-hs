{-# LANGUAGE DeriveGeneric #-}
module Token
  ( Token (..)
  , getRandomUUID
  )where

import           Data.Aeson
import           Data.UUID
import           Data.UUID.V4
import           GHC.Generics

newtype Token = Token
  { token :: UUID
  } deriving (Show, Eq, Generic)


instance ToJSON Token

getRandomUUID :: IO UUID
getRandomUUID = nextRandom
