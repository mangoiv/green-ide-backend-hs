{-# LANGUAGE DeriveGeneric #-}
module EvaluationRequest where

import           Data.Aeson
import           Data.Text
import           Data.UUID
import           Evaluation
import           GHC.Generics

data Configurations = MkConfigurations
  { base    :: Configuration
  , compare :: [Configuration]
  }
  deriving (Eq, Show, Generic)

type Configuration = [Int]

data EvaluationRequest = MkEvaluationRequest
  { requestToken :: UUID
  , currentPath  :: FilePath
  , methods      :: [Method]
  }
  deriving (Eq, Show, Generic)

data Method = MkMethod
  { name           :: Text
  , location       :: Location
  , configurations :: Configurations
  }
  deriving (Eq, Show, Generic)

data Location = MkLocation
  { start :: LineAndChar
  , end   :: LineAndChar
  , path  :: FilePath
  }
  deriving (Eq, Show, Generic)

data LineAndChar = MkLineAndChar
  { ln  :: Int
  , chr :: Int
  }
  deriving (Eq, Show, Generic)

instance FromJSON EvaluationRequest
instance FromJSON Method
instance FromJSON Location
instance FromJSON LineAndChar
instance FromJSON Configurations
